$(function() {
    $("#seat-booking").on('submit', function(e){
        e.preventDefault();
        let url = this.action;
        $.ajax({
            url: url,
            type:'POST',
            success: function(response){
                if(response['status']==400){
                    alert("please select valid value")
                }else{
                    $("body").html(response['html']);
                    $("#seat-booking").hide();
                    $("#success_msg").show();
                }
            }
        })
    })
})

//AJAX for book seat
function event_book(value){
    let url = $(value).attr("data-target")
    let seat_success_id = "seat_success_"+value.id
    let text = "Are you sure? you want to book this seat.";
    if (confirm(text) == true) {
        $.ajax({
            url: url,
            type:'POST',
            success: function(response){
                if(response['STATUS']==200){
                    $("#"+seat_success_id).text(response['DETAIL']);
                    $(value).hide();
                }else{
                    alert("Something went wrong");
                }
            }
        })
    }
}

//AJAX for cancel seat
function cancel_event(value){
      let url = $(value).attr("data-target")
      debugger;
      if  ($(value).hasClass('close-btn')){
         $.ajax({
            url: url,
            type:'GET',
            success: function(response){
                if(response['STATUS']==200){
                    $(".my-event-list").html(response['HTML'])
                }else{
                    alert("Something went wrong");
                }
            }
         })
      }else{
          let seat_success_id = "seat_success_"+value.id
          let text = "Are you sure? you want to cancel your booked seat.";
          if (confirm(text) == true) {
                $.ajax({
                    url: url,
                    type:'POST',
                    success: function(response){
                        if(response['STATUS']==200){
                            $("#"+seat_success_id).text(response['DETAIL'])
                            $(value).hide()
                        }else{
                            alert("Something went wrong");
                        }
                    }
                })
          }
      }

}