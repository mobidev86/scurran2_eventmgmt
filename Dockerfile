FROM python:3
ENV PYTHONUNBUFFERED 1	

RUN python -m venv /opt/venv
# Enable venv
ENV PATH="/opt/venv/bin:$PATH"

WORKDIR /BookingManagement
COPY requirements.txt /BookingManagement/
RUN pip install -r requirements.txt
COPY . /BookingManagement