"""
    Account Management Docs
"""

from django.apps import AppConfig


class AccountmgmtConfig(AppConfig):

    """
        Account Management Class Docs
    """

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'accountmgmt'
