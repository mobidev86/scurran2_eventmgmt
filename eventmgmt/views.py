"""
    view.py file
"""
from django.shortcuts import render, redirect
from django.views import View
from django.http import JsonResponse
from django.template.loader import render_to_string

from accountmgmt.models import UserProfile
from eventmgmt.models import (EventPlanPrice, EventBookingManagement)


class LoginRequiredMixin(View):
    """
        Login Required decorator class
    """
    def dispatch(self, request, *args, **kwargs):
        """
            Login Required decorator class
        """
        if not request.user.is_authenticated:
            return redirect('accountmgmt:login')
        return super().dispatch(request, args, *kwargs)


class BookedEventDetailView(LoginRequiredMixin):
    """
        BookedEventDetailView class docs
    """

    def render_template(self, template_path, context=None):
        """
        	render template
        """
        return render(self.request, template_path, context)


    def get(self, request, *args, **kwargs):
        """
            Get Method
        """
        context = {}
        context.update({
            "all_booked_event": []
        })

        if request and request.user:
            if request.user.is_authenticated and request.user.userprofile:
                context.update({
                    "all_booked_event": EventBookingManagement.objects.filter(
                        event_user_profile_id=request.user.userprofile),
                })

        return self.render_template(template_path="event/my-event-details.html",
                                    context=context)


class NewBookingEventView(LoginRequiredMixin):
    """
        NewBookingEventView docs class
    """
    def render_template(self, template_path, context=None):
        """
        	renders template
        """
        return render(self.request, template_path, context)


    def get(self, request, *args, **kwargs):
        """
            get method docs
        """
        context = {}
        context.update({
            "all_new_event_booking": [],
            "all_booked_event": []
        })

        if request and request.user:
            if request.user.is_authenticated and request.user.userprofile:
                context.update({
                    "all_new_event_booking": EventPlanPrice.objects.filter(
                        event_id__is_active=True),
                    "all_booked_event": list(EventBookingManagement.objects.filter(
                        event_user_profile_id=request.user.userprofile).values_list(
                        'evt_plan_price_id__id', flat=True))
                })
        return self.render_template(template_path="event/new-event-booking-details.html",
                                    context=context)


class SeatBookingView(LoginRequiredMixin):
    """
        seat booking
    """

    def post(self, *args, **kwargs):
        """
            post method docs
        """
        event_id = self.kwargs.get('event_id')
        plan_type = EventPlanPrice.objects.get(id=event_id)
        username = self.request.user.username
        user = UserProfile.objects.get(username=username)
        event = plan_type.event_id

        obj, created = EventBookingManagement.objects.get_or_create(event_id=event,
                event_user_profile_id=user, defaults={'evt_plan_price_id': plan_type})

        if created:
            if event.left_seats > 0:
                event.left_seats -= 1
                event.booked_seats += 1
                event.save()
            else:
                return JsonResponse({
                    "DETAIL": "Sorry! all seats of events are booked.",
                    "STATUS": 200,

                })
        obj.evt_plan_price_id = plan_type
        obj.save()
        return JsonResponse({
            "DETAIL": "You ticket is booked.",
            "STATUS": 200,
        })


class SeatCancelView(LoginRequiredMixin):
    """
        Cancel  booking
    """

    def get(self, *args, **kwargs):
        context = {}
        context.update({
            "request":  self.request,
            "all_booked_event": EventBookingManagement.objects.filter(
                event_user_profile_id=self.request.user.userprofile
            )
        })
        html = render_to_string("common/my-event-list.html", context)
        return JsonResponse({
            "STATUS": 200,
            'HTML': html
        })

    def post(self, *args, **kwargs):
        """
            post method docs
        """
        booked_event_id = self.kwargs.get('booked_event_id')
        obj = EventBookingManagement.objects.get(id=booked_event_id)
        event = obj.event_id
        event.left_seats += 1
        event.booked_seats -= 1
        event.save()
        obj.delete()
        return JsonResponse({
            "DETAIL": "Seat cancelled successfully.",
            "STATUS": 200
        })
