"""
  Urls file docs
"""
from django.urls import path
from eventmgmt import views

urlpatterns = [
    path("my/event/list", views.BookedEventDetailView.as_view(), name="my-event-list"),

    path("new/event/booking/list", views.NewBookingEventView.as_view(), name="new-event-booking"),

    path('new/event/booking/seat/<int:event_id>/', views.SeatBookingView.as_view(),
         name="seat-booking"),
    path('new/event/cancel/seat/<int:booked_event_id>/', views.SeatCancelView.as_view(),
         name="seat-cancel"),

]
