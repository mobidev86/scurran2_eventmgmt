"""
    Used To manage model
"""
from django.db import models
from accountmgmt.models import UserProfile


EVENT_STATUS_CHOICES = (
    ("upcoming", ""),
    ("", ""),
)


class Event(models.Model):
    """
        Event class docs
    """
    event_name = models.CharField("Event Name", max_length=100)
    event_start_date = models.DateField("Event Start Date", null=True, blank=True)
    event_end_date = models.DateField("Event End Date", null=True, blank=True)

    no_of_seats = models.IntegerField("Number of Seats")

    booked_seats = models.IntegerField("Booked Seats", null=True, blank=True)
    left_seats = models.IntegerField("Left Seats", null=True, blank=True)

    is_active = models.BooleanField("Is Active", default=False,
                                    help_text="Used to manage event status")

    def __str__(self):
        return str(self.event_name)


class EventPlan(models.Model):
    """
        EventPlan class docs
    """
    event_plan_name = models.CharField("Event Plan Name", max_length=100)

    def __str__(self):
        return str(self.event_plan_name)


class EventPlanPrice(models.Model):
    """
        EventPlanPrice class docs
    """
    event_id = models.ForeignKey(Event, on_delete=models.PROTECT)
    event_type = models.ForeignKey(EventPlan, on_delete=models.PROTECT)
    event_price = models.FloatField("Event Price")

    class Meta:
        """
            Meta class docs
        """
        unique_together = ("event_id", "event_type",)

    def __str__(self):
        return str(self.event_type)


class EventBookingManagement(models.Model):
    """
        EventBookingManagement class docs
    """
    event_id = models.ForeignKey(Event, on_delete=models.PROTECT)
    evt_plan_price_id = models.ForeignKey(EventPlanPrice, on_delete=models.PROTECT)
    event_user_profile_id = models.ForeignKey(UserProfile, on_delete=models.PROTECT)

    def __str__(self):
        return str(self.event_id)
