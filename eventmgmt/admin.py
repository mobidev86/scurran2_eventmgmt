"""
    admin.py file
"""
from django.contrib import admin
from eventmgmt.models import Event, EventPlan, EventPlanPrice, \
    EventBookingManagement


class EventAdmin(admin.ModelAdmin):
    """
        EventAdmin class docs
    """
    list_display = ["event_name", "event_start_date", "event_end_date",
                    "no_of_seats", "booked_seats", "left_seats", "is_active"]
    search_fields = ["event_name", ]
    readonly_fields = ["booked_seats", "left_seats"]


class EventPlanAdmin(admin.ModelAdmin):
    """
        EventPlanAdmin class docs
    """
    list_display =["event_plan_name",]


class EventPlanPriceAdmin(admin.ModelAdmin):
    """
        EventPlanPriceAdmin class docs
    """
    list_display = ["event_id", "event_type", "event_price"]


class EventBookingManagementAdmin(admin.ModelAdmin):
    """
        EventBookingManagementAdmin class docs
    """
    list_display = ["event_id", "evt_plan_price_id", "event_user_profile_id"]


admin.site.register(Event, EventAdmin)
admin.site.register(EventPlan, EventPlanAdmin)
admin.site.register(EventPlanPrice, EventPlanPriceAdmin)
admin.site.register(EventBookingManagement, EventBookingManagementAdmin)
