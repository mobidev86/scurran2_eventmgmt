"""
    apps.py file
"""
from django.apps import AppConfig


class EventmgmtConfig(AppConfig):
    """
        EventmgmtConfig class docs
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'eventmgmt'

    def ready(self):
        """
            ready function docs
        """
        import eventmgmt.signals
