"""
    event signal docs
"""
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Event


@receiver(post_save, sender=Event)
def save_events_details(instance, created, *args, **kwargs):
    """
    save_events_details  docs
    """
    if created:
        instance.booked_seats = 0
        instance.left_seats = instance.no_of_seats
        instance.save()
